import NProgress from "nprogress";

export default function({ $axios, store }) {
  $axios.interceptors.request.use(
    function(request) {
      // Do something before request is sent
      NProgress.start();
      return request;
    },
    function(error) {
      // Do something with request error
      console.log(error);
      NProgress.done();
      return Promise.reject(error);
    }
  );

// Add a response interceptor
  $axios.interceptors.response.use(
    function(response) {
      NProgress.done();
      return response;
    },
    function(error) {
      // Do something with response error
      console.log(error);
      NProgress.done();
      return Promise.reject(error);
    }
  );

}
