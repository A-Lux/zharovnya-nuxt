import collect from 'collect.js';

export const state = () => ({
  apiEndpoint: 'https://zharovnya.kz/api/',
  storageEndpoint: 'https://zharovnya.kz/',
  categories: [],
  products: [],
  page: 1,
  cart: [],
  banners: []
})

export const mutations = {
  SET_CATEGORIES(state, value) {
    state.categories = value
  },
  SET_PRODUCTS(state, value) {
    state.products = value
  },
  SET_BANNERS(state, value) {
    state.banners = value
  },
  ADD_PRODUCT_TO_CART(state, value) {
    state.cart.push(value)
  },
  REMOVE_PRODUCT_FROM_CART(state, id) {
    state.cart = collect(state.cart).filter((value, key) => value.id != id)
  },
  UPDATE_PRODUCT_FROM_CATALOG(state, product) {
    let exists = collect(state.cart).where('id', product.id).first();
    exists.amount += product.amount
  },
  UPDATE_PRODUCT_FROM_CART(state, product) {
    let exists = collect(state.cart).where('id', product.id).first();
    exists.amount = product.amount
  },
  PURGE_CART(state) {
    state.cart = []
  }
}

export const getters = {
  categories: state => {
    return collect(state.categories);
  },
  products: state => {
    return collect(state.products);
  },
  storage: state => {
    return state.storageEndpoint
  },
  api: state => {
    return state.apiEndpoint
  },
  cart: state => {
    return state.cart
  },
  banners: state => {
    return state.banners
  },
}

export const actions = {
  getCategories({ commit, state }) {
    this.$axios.$get(state.apiEndpoint + 'categories')
      .then(response => {
        commit('SET_CATEGORIES', response.data)
      })
  },
  getBanners({ commit, state }) {
    this.$axios.$get(state.apiEndpoint + 'banners')
      .then(response => {
        commit('SET_BANNERS', response.data)
      })
  },
  getProducts({ commit, state }) {
    this.$axios.$get(state.apiEndpoint + 'products/all')
      .then(response => {
        commit('SET_PRODUCTS', response.data)
      })
  },
  getProductsNew({ commit, state }, category) {
    this.$axios.$get(state.apiEndpoint + 'product/new')
      .then(response => {
        commit('SET_PRODUCTS', response.data)
      })
  },
  getProductsTop({ commit, state }, category) {
    this.$axios.$get(state.apiEndpoint + 'product/top')
      .then(response => {
        commit('SET_PRODUCTS', response.data)
      })
  },
  getProductsDay({ commit, state }, category) {
    this.$axios.$get(state.apiEndpoint + 'product/day')
      .then(response => {
        commit('SET_PRODUCTS', response.data)
      })
  },
  getProductsDiscount({ commit, state }, category) {
    this.$axios.$get(state.apiEndpoint + 'product/discount')
      .then(response => {
        commit('SET_PRODUCTS', response.data)
      })
  },
  addToCart({ commit, state }, product) {
    console.log(collect(state.cart).where('id', product.id).first())
    let exists = collect(state.cart).where('id', product.id).first();
    if(exists) {
      commit('UPDATE_PRODUCT_FROM_CATALOG', product)
    }
    else {
      commit('ADD_PRODUCT_TO_CART', product)
    }
  },
  removeFromCart({ commit, state }, product) {
    commit('REMOVE_PRODUCT_FROM_CART', product)
  },
  updateProductInCart({ commit, state }, product) {
    commit('UPDATE_PRODUCT_FROM_CART', product) //TODO UPDATE
  },
  purgeCart({ commit, state }) {
    commit('PURGE_CART')
  }
}
